'use strict';

const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const AddressModel = Joi.object({
  name: Joi.string().required(),
  street: Joi.string().required(),
  country: Joi.string().required(),
  postal_code: Joi.string().required(),
  city: Joi.string().required()
});

const BookingModel = Joi.object({
  client_reference: Joi.string().required(),
  cargo_details: Joi.array().items(
      Joi.object({
        shipper_info: AddressModel.required(),
        packages: Joi.array().items(
            Joi.object({
              quantity: Joi.number().min(1).required(),
              weight: Joi.number().min(0),
              dangerous_goods: Joi.boolean().default(false)
            })
        )
      })
  ),
  consignee_info: AddressModel.required()
});

const bookings = [1,2,3].map(id => ({
  client_reference: `client-reference-${id}`,
  cargo_details: [
    {
      shipper_info: {
        name: 'Shipper Name',
        street: 'Shipper Street 123',
        country: 'The Netherlands',
        postal_code: '1234AA',
        city: 'Rotterdam'
      },
      packages: [
        {
          quantity: 5,
          weight: 100000,
          dangerous_goods: false
        },
        {
          quantity: 5,
          weight: 100000,
          dangerous_goods: true
        }
      ]
    }
  ],
  consignee_info: {
    name: 'Consignee Name',
    street: 'Consignee Street 123',
    country: 'The Netherlands',
    postal_code: '1234AA',
    city: 'Rotterdam'
  },
}));

const init = async () => {

  const server = Hapi.server({
    port: 3000,
    host: 'localhost',
    routes: {
      validate: {
        failAction: async (request, h, err) => {
          throw Boom.badRequest(err.toString(), { error: err });
        }
      },
      response: {
        failAction: async (request, h, err) => {
          throw Boom.internal(err.toString(), { error: err });
        }
      },
      cors: {
        origin: ['*']
      }
    }
  });

  server.route({
    method: 'GET',
    path: '/bookings',
    handler: () => {
      return bookings;
    }
  });

  server.route({
    method: 'POST',
    path: '/bookings',
    options: {
      validate: {
        payload: BookingModel.options({stripUnknown: true})
      }
    },
    handler: (request, h) => {
      bookings.push(request.payload);
      return h.response().code(201);
    }
  });

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

  console.log(err);
  process.exit(1);
});

init();